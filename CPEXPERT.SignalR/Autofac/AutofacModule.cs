﻿using Autofac;
using CPEXPERT.SignalR.Services;
using CPEXPERT.SignalR.Services.Abstracts;
using Serilog;

namespace CPEXPERT.SignalR.Autofac
{
	public class AutofacModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);
			builder.RegisterType<CommandService>().As<ICommandService>().InstancePerLifetimeScope();
			builder.RegisterType<CommunicationService>().As<ICommunicationService>().InstancePerLifetimeScope();
			builder.RegisterType<SystemRemoteMethodsService>().As<ISystemRemoteMethodsService>().InstancePerLifetimeScope();
			Log.Debug("[CPEXPERT.SignalR.Autofac].Load(...)");
		}
	}
}
