﻿using System;
using System.Threading.Tasks;

namespace CPEXPERT.SignalR.Services.Abstracts
{
	public interface ISystemRemoteMethodsService : ISignalRService
	{
		Task RefreshUserProfile(Guid userAppKey);
	}
}