﻿namespace CPEXPERT.SignalR.Services.Abstracts
{
	public interface ISignalRService
	{
		string Name { get; }
	}
}