﻿using System.Threading.Tasks;
using CPEXPERT.SignalR.Contracts;

namespace CPEXPERT.SignalR.Services.Abstracts
{
	public interface ICommandService : ISignalRService
	{
		Task ExecuteSuccess(CommandMessagesToUserData request);
		Task ExecuteValidationFailure(CommandMessagesToUserData request);
		Task Test();
	}
}