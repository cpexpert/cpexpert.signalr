﻿using System;
using System.Threading.Tasks;
using CPEXPERT.Common.ContractsData.Message;

namespace CPEXPERT.SignalR.Services.Abstracts
{
	public interface ICommunicationService : ISignalRService
	{
		Task ShowMessageToUsers(MessageData message, Guid[] usersAppKeys);
		Task Test();
	}
}