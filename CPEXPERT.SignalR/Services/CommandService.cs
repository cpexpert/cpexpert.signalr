﻿using System;
using System.Threading.Tasks;
using CPEXPERT.SignalR.Contracts;
using CPEXPERT.SignalR.Hubs;
using CPEXPERT.SignalR.Services.Abstracts;
using Microsoft.AspNetCore.SignalR;
using Serilog;

namespace CPEXPERT.SignalR.Services
{
	public class CommandService : ICommandService
	{
		public string Name => "Command";
		private readonly IHubContext<CommandHub> _hub;
		private readonly ILogger _logger;

		public CommandService(IHubContext<CommandHub> hub, ILogger logger)
		{
			_hub = hub;
			_logger = logger;
		}


		public Task ExecuteSuccess(CommandMessagesToUserData request)
		{
			try
			{
				return _hub.Clients.All.SendAsync($"Hub.{Name}.ExecuteSuccess", request);
			}
			catch (Exception e)
			{
				_logger.Error(e, e.Message);
				throw;
			}
		}

		public Task ExecuteValidationFailure(CommandMessagesToUserData request)
		{
			try
			{
				return _hub.Clients.All.SendAsync($"Hub.{Name}.ExecuteValidationFailure", request);
			}
			catch (Exception e)
			{
				_logger.Error(e, e.Message);
				throw;
			}
		}

		public Task Test()
		{
			return _hub.Clients.All.SendAsync($"Hub.{Name}.Test", "SUCCESS!!!");
		}
	}
}