﻿using System;
using CPEXPERT.SignalR.Hubs;
using CPEXPERT.SignalR.Services.Abstracts;
using Microsoft.AspNetCore.SignalR;
using Serilog;
using System.Threading.Tasks;
using CPEXPERT.Common.ContractsData.Message;

namespace CPEXPERT.SignalR.Services
{
	public class CommunicationService : ICommunicationService
	{
		public string Name => "Communication";
		private readonly IHubContext<CommunicationHub> _hub;
		private readonly ILogger _logger;

		public CommunicationService(IHubContext<CommunicationHub> hub, ILogger logger)
		{
			_hub = hub;
			_logger = logger;
		}

		public Task ShowMessageToUsers(MessageData message, Guid[] usersAppKeys)
		{
			try
			{
				return _hub.Clients.All.SendAsync($"Hub.{Name}.ShowMessageToUsers",
				new {
					Message = message,
					UsersAppKeys = usersAppKeys
				});
			}
			catch (Exception e)
			{
				_logger.Error(e, e.Message);
				throw;
			}
		}

		public Task Test()
		{
			return _hub.Clients.All.SendAsync($"Hub.{Name}.Test", "SUCCESS!!!");
		}
	}
}