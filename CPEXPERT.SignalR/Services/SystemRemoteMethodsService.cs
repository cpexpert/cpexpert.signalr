﻿using System;
using System.Threading.Tasks;
using CPEXPERT.SignalR.Hubs;
using CPEXPERT.SignalR.Services.Abstracts;
using Microsoft.AspNetCore.SignalR;
using Serilog;

namespace CPEXPERT.SignalR.Services
{
	public class SystemRemoteMethodsService : ISystemRemoteMethodsService
	{
		public string Name => "SystemRemoteMethods";
		private readonly IHubContext<SystemRemoteMethodsHub> _hub;
		private readonly ILogger _logger;

		public SystemRemoteMethodsService(IHubContext<SystemRemoteMethodsHub> hub, ILogger logger)
		{
			_hub = hub;
			_logger = logger;
		}

		public Task RefreshUserProfile(Guid userAppKey)
		{
			try
			{
				return _hub.Clients.All.SendAsync($"Hub.{Name}.RefreshUserProfile", userAppKey);
			}
			catch (Exception e)
			{
				_logger.Error(e, e.Message);
				throw;
			}
		}
	}
}