﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Serilog;

namespace CPEXPERT.SignalR.Hubs
{
	public class CommunicationHub : Hub
	{
		private readonly ILogger _logger;

		public CommunicationHub(ILogger logger)
		{
			_logger = logger;
		}

		public override Task OnConnectedAsync()
		{
			_logger.Debug($"Connect: {Context.ConnectionId}");
			return base.OnConnectedAsync();
		}

		public override Task OnDisconnectedAsync(Exception exception)
		{
			_logger.Warning($"Disconnect: {Context.ConnectionId}");
			return base.OnDisconnectedAsync(exception);
		}
	}
}