﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.ContractsData.Message;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CPEXPERT.SignalR.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public class HubController : ControllerBase
	{
		[AllowAnonymous]
		[HttpGet("GetDefinedHubsUrls")]
		public Task<Response<List<string>>> GetDefinedHubsUrls()
		{
			var baseWebApiUrl = Common.Helpers.ConfigurationHelper.GetConfigSection<string>("BaseWebApiUrl");
			return Task.FromResult(new Response<List<string>>
			{
				CanCache = false,
				Created = DateTime.Now,
				Id = Guid.NewGuid(),
				Messages = new List<MessageData>(),
				RequestCreated = DateTime.Now,
				RequestId = Guid.NewGuid(),
				ResponseData = new List<string>
				{
					$"{baseWebApiUrl}/hubs/command",
					$"{baseWebApiUrl}/hubs/communication",
					$"{baseWebApiUrl}/hubs/systemremotemethods",
				}
			});
		}
	}
}