﻿using System;
using System.Threading.Tasks;
using CPEXPERT.Common.Contracts;
using CPEXPERT.SignalR.Services.Abstracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CPEXPERT.SignalR.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public class SystemRemoteMethodsController : ControllerBase
	{
		private readonly ISystemRemoteMethodsService _service;

		public SystemRemoteMethodsController(ISystemRemoteMethodsService service)
		{
			_service = service;
		}

		[HttpPost("RefreshUserProfile")]
		public async Task<IActionResult> RefreshUserProfile([FromBody] Request<Guid> request)
		{
			try
			{
				if (request?.RequestData != null && request.RequestData != Guid.Empty)
				{
					await _service.RefreshUserProfile(request.RequestData);
					return Ok();
				}

				throw new Exception("Request not validate");
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}
		}
	}
}