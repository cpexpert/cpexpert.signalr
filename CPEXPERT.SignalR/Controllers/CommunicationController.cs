﻿using System.Threading.Tasks;
using CPEXPERT.SignalR.Services.Abstracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace CPEXPERT.SignalR.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public class CommunicationController : ControllerBase
	{
		private readonly ICommunicationService _service;
		private readonly ILogger _logger;

		public CommunicationController(ICommunicationService service, ILogger logger)
		{
			_service = service;
			_logger = logger;
		}

		[HttpPost("Test")]
		[AllowAnonymous]
		public async Task<IActionResult> Test()
		{
			await _service.Test();
			return Ok();
		}
	}
}