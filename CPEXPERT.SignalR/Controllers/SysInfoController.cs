﻿using System.Threading.Tasks;
using AutoMapper;
using CPEXPERT.Common.Enums;
using CPEXPERT.Common.Structures;
using CPEXPERT.Common.WebApi.Controllers;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace CPEXPERT.SignalR.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public class SysInfoController : SysInfoAbstractController
	{
		public SysInfoController(IMediator mediator, ILogger logger, IMapper mapper) : base(mediator, logger, mapper)
		{
		}

		[HttpGet("GetModuleInformation")]
		public override Task<ModuleInfo> GetModuleInformation()
		{
			return Task.FromResult(new ModuleInfo
			{
				AssemblyName = "test",
				AssemblyVersion = "ssss",
				Copyright = "xxxxx",
				IsLicensedModule = false,
				IsRequiredSystemModule = true,
				ModuleName = "test",
				ModuleType = AppModules.Core
			});
		}
	}
}
