﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.Enums;
using CPEXPERT.SignalR.Contracts;
using CPEXPERT.SignalR.Services.Abstracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CPEXPERT.SignalR.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public class CommandController : ControllerBase
	{
		private readonly ICommandService _service;

		public CommandController(ICommandService service)
		{
			_service = service;
		}

		[HttpPost("ExecuteSuccess")]
		public async Task<IActionResult> ExecuteSuccess([FromBody] Request<CommandMessagesToUserData> request)
		{
			try
			{
				if (request?.RequestData != null && request.RequestData.UserAppKey != Guid.Empty)
				{
					if (request.RequestData.Messages.Any(x => x.Type == MessageTypes.Error || x.Type == MessageTypes.Warning))
					{
						throw new Exception("Can't sent success message to user, error on warning exist on message list");
					}
					await _service.ExecuteSuccess(request.RequestData);
					return Ok();
				}

				throw new Exception("Request not validate");
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}
		}

		[HttpPost("ExecuteValidationFailure")]
		public async Task<IActionResult> ExecuteValidationFailure([FromBody] Request<CommandMessagesToUserData> request)
		{
			try
			{
				if (request?.RequestData?.Messages != null && request.RequestData.UserAppKey != Guid.Empty)
				{
					if (request.RequestData.Messages.Any(x => x.Type == MessageTypes.Error || x.Type == MessageTypes.Warning))
					{
						await _service.ExecuteSuccess(request.RequestData);
						return Ok();
					}
				}

				throw new Exception("Can't sent error message to user, error or warning no exist on message list");
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}
		}

		[HttpPost("Test")]
		[AllowAnonymous]
		public async Task<IActionResult> Test()
		{
			await _service.Test();
			return Ok();
		}
	}
}