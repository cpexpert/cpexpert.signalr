﻿using System;
using CPEXPERT.Common.ContractsData.Message;

namespace CPEXPERT.SignalR.Contracts
{
	public class CommandMessagesToUserData
	{
		public MessageData[] Messages { get; set; }
		public Guid UserAppKey { get; set; }
	}
}